import React from 'react'
import ReactDOM from 'react-dom'
import Highlight from 'react-highlight.js'
import Accordion, { Collapsible } from '../../src'
import './examples.css'


const PROPERTIES = [
]

class Example extends React.Component {
    render() {
        const items = [
            {
                title: 'London',
                content: <p>London, the capital of England and the United Kingdom, is a 21st-century city with history stretching back to Roman times. At its centre stand the imposing Houses of Parliament, the iconic ‘Big Ben’ clock tower and Westminster Abbey, site of British monarch coronations. Across the Thames River, the London Eye observation wheel provides panoramic views of the South Bank cultural complex, and the entire city.</p>
            },
            {
                title: 'Paris',
                content: <p>Paris, France's capital, is a major European city and a global center for art, fashion, gastronomy and culture. Its 19th-century cityscape is crisscrossed by wide boulevards and the River Seine. Beyond such landmarks as the Eiffel Tower and the 12th-century, Gothic Notre-Dame cathedral, the city is known for its cafe culture and designer boutiques along the Rue du Faubourg Saint-Honoré.</p>
            },
            {
                title: 'Madrid',
                content: <p>Madrid, Spain's central capital, is a city of elegant boulevards and expansive, manicured parks such as the Buen Retiro. It’s renowned for its rich repositories of European art, including the Prado Museum’s works by Goya, Velázquez and other Spanish masters. The heart of old Hapsburg Madrid is the portico-lined Plaza Mayor, and nearby is the baroque Royal Palace and Armory, displaying historic weaponry.</p>
            },
            {
                title: 'Iasi',
                content: <p>Iași is a university city in eastern Romania, near the border with Moldova. In the center is the huge St. Paraschiva Metropolitan Cathedral, a 19th-century Orthodox church built in Italian Renaissance style. Nearby, the Three Hierarchs Monastery has an exterior decorated with delicate, Moorish-style stone carvings. Backed by Palas Park’s manicured gardens, the Palace of Culture is a vast neo-Gothic building.</p>
            },
        ]

        const nested = [
            {
                title: 'Parent',
                content: <Accordion items={items} />
            },
            {
                title: 'Second parent',
                content: <Accordion items={items} />
            },
        ]

        return (
            <div className="container">
                <h1>awesome-react-accordion</h1>
                <div className="example">
                    <h3>Default</h3>
                    <Accordion
                        items={items}
                        speed={500}
                    />
                    <Highlight language="html">
                        {'<Accordion\nitems={items}\nspeed={500}\n/>'}
                    </Highlight>
                </div>

                <div className="example">
                    <h3>Multiple</h3>
                    <Accordion
                        items={items}
                        multiple={true}
                    />
                    <Highlight language="html">
                        {'<Accordion\nitems={items}\nmultiple={true}\n/>'}
                    </Highlight>
                </div>

                <div className="example">
                    <h3>Reversed</h3>
                    <Accordion
                        reversed={true}
                        items={items}
                        speed={500}
                    />
                    <Highlight language="html">
                        {'<Accordion\nitems={items}\nspeed={500}\nreversed={true}\n/>'}
                    </Highlight>
                </div>

                <div className="example">
                    <h3>Nesting</h3>
                    <Accordion
                        items={nested}
                        speed={500}
                    />
                    <Highlight language="html">
                        {'<Accordion\nitems={items}\nspeed={500}\n/>'}
                    </Highlight>
                </div>

                <div className="example">
                    <h3>Collapsible/Single item</h3>
                    <Collapsible title={'Iasi'}><p>Iași is a university city in eastern Romania, near the border with Moldova. In the center is the huge St. Paraschiva Metropolitan Cathedral, a 19th-century Orthodox church built in Italian Renaissance style. Nearby, the Three Hierarchs Monastery has an exterior decorated with delicate, Moorish-style stone carvings. Backed by Palas Park’s manicured gardens, the Palace of Culture is a vast neo-Gothic building.</p></Collapsible>
                    <Highlight language="html">
                        {'<Collapsible title={"title_here"}>content_here</Collapsible>'}
                    </Highlight>
                </div>

            </div>
        )
    }
}

ReactDOM.render(<Example />, document.getElementById('app'))