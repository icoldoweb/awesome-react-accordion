An accordion component build with React.

## Documentation
#### Installation

    yarn add awesome-react-accordion
    npm install awesome-react-accordion

#### Example
```js
    import Accordion, {Collapsible} from "awesome-react-accordion"
    
	render(){
		const items = [
			{
				title: 'Title 1',
				content: 'Content 1'
			},
			{
				title: 'Title 2',
				content: 'Content 2'
			},
			{
				title: 'Title 3',
				content: 'Content 3'
			},
		]
	
		return (
			// Multiple items
			<Accordion items={items} />

			// If you only want one item
			<Collapsible title="Single item title">Single item content</Collapsible>
		)
	}
```
> For complete examples on how each prop works check the examples included (see [Development](#development))
#### Props
**items** (type: *array*,  default: *[]*)
An array of items to be shown.

**multiple** (type: *boolean*,  default: *false*)
Allow multiple items to be visible

**speed** (type: *int*,  default: *350*)
The animation speed in milliseconds


## Development
Run examples

    git clone git@bitbucket.org:icoldoweb/awesome-react-accordion.git
    cd awesome-react-accordion
    yarn
    yarn start
    // a new tab will open in your default browser on http://localhost:8080
    
