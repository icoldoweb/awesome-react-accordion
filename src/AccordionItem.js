import React from 'react'
import { AccordionDefaultItem, AccordionContent } from './styles'
import { DEFAULT_SPEED } from './Accordion'
import { Transition } from 'react-transition-group';

export default class AccordionItem extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        const {
            active,
            className,
            title,
            children,
            onClick,
            speed,
            reversed,
            height,
        } = this.props

        const classNames = [
            'ar-accordion-item',
            className || null,
            active ? 'active' : null
        ]
            .filter(i => i)
            .join(' ')

        const defaultStyle = {
            transition: `all ${speed || DEFAULT_SPEED}ms ease-in-out`,
            overflow: 'hidden',
            height: (height || 0) + 'px',
        }
        const innerHeight = this.ref ? this.ref.scrollHeight + 'px' : 0

        const transitionStyles = {
            entering: {
                height: innerHeight,
            },
            entered: {
                height: 'unset',
            },
            exiting: {
                height: innerHeight,
            },
            exited: {
            },
        };

        return (
            <AccordionDefaultItem
                className={classNames}
                onClick={onClick}
                {...(reversed ? { style: { flexDirection: 'column-reverse' } } : {})}
            >
                <div className="ar-accordion-item-title">{title}</div>
                <Transition in={active}
                    timeout={{
                        enter: speed || DEFAULT_SPEED,
                        exit: 0,
                    }}
                >
                    {state => (
                        <div
                            onClick={e => e.stopPropagation()}
                            ref={(el) => this.ref = el}
                            className="ar-accordion-item-content-wrapper"
                            style={{
                                ...defaultStyle,
                                ...transitionStyles[state]
                            }}>
                            <div className="ar-accordion-item-content">{children}</div>
                        </div>
                    )}
                </Transition>
            </AccordionDefaultItem >
        )
    }
}