import React from 'react'
import AccordionItem from './AccordionItem'
import { AccordionWrapper } from './styles'

export const DEFAULT_SPEED = 350

export class Collapsible extends React.Component {
    render() {
        const {
            title,
            children,
            multiple,// ignore multiple
            ...rest
        } = this.props
        const items = [{
            title,
            content: children,
        }]

        return (
            <Accordion items={items} {...rest} />
        )
    }
}

export default class Accordion extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            activeIndex: props.activeIndex || null,
            activeIndexes: {}
        }
        this.speed = props.speed || DEFAULT_SPEED
    }

    handleClick = (index) => {
        const { onClick, multiple, speed } = this.props
        const { activeIndex, activeIndexes } = this.state
        if (multiple) {
            this.setState({
                activeIndexes: {
                    ...activeIndexes,
                    [index]: !activeIndexes[index]
                }
            }, () => {
                if (onClick) onClick(index)
            })
            return
        }
        this.setState({ activeIndex: activeIndex == index ? null : index }, () => {
            if (onClick) onClick(index)
        })
    }

    render() {

        const { items,
            className,
            childrenClassName,
            multiple,
            speed,
            reversed,
            height,
            id = '',
        } = this.props
        const { activeIndex, activeIndexes } = this.state
        let classNames = ['ar-accordion', className]
        classNames = classNames.filter(i => i).join(' ')

        return (
            <AccordionWrapper className={classNames}>
                {
                    (items || []).map((item, index) => {
                        const active = multiple
                            ? activeIndexes[index]
                            : index == activeIndex
                        return <AccordionItem
                            key={`accordion-item-${id}-${index}`}
                            active={active}
                            onClick={this.handleClick.bind(this, index)}
                            className={childrenClassName}
                            speed={speed}
                            title={item.title}
                            reversed={reversed || false}
                            height={height}
                        >{item.content}</AccordionItem>
                    })
                }
            </AccordionWrapper>
        )
    }
}