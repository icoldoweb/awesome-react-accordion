import styled from 'styled-components'


export const AccordionWrapper = styled.div`
`

export const AccordionDefaultItem = styled.div`
    display: flex;
    flex-direction: column;
`

export const AccordionContent = styled.div`
    overflow: hidden; /* scrollHeight has the wrong value without it */
    height: 0;
    transition: all;
`